import tkinter as tk
import re

def verificar(): 
  ventana = tk.Toplevel(root, width=400)
  ventana.title("vamos a evaluar")
  ventana.geometry("200x100")
  tk.Label(ventana,text=pwd_input.get())
  v = pwd_input.get()
  p = re.compile("^(?=.{7,15}$)(?=.*[0-9].*)(?=.*[a-z].*)(?=.*[A-Z].*)(?=.*[{}\\[\\]\\-+.,():;{}_].*)")
  if p.match(v) and v == confirmar_pwd_input.get():  
    tk.Label(ventana, text="Las contraseñas coinciden y Cumplen con la Condiciones").pack()
  else:
    tk.Label(ventana, text="Las Contaseñas no coinciden o no cumplen las condiciones").pack()

root=tk.Tk()
root.title("Alta de Usuario")
root.geometry("500x430")

# Aviso
label_aviso = tk.Label(root, text="Porfavor ingrese su pwd y correo electrónico", font=(
    "Arial", 14), bg="#3174B8", fg="white")

titulo_label= tk.Label(root,text="Alta de usuario")
titulo_label.pack(pady=10)

correo_label= tk.Label(root, text="Correo Electrónico")
correo_input= tk.Entry(root, font=("Arial", 14))

# Sección de pwd
pwd_label = tk.Label(root, text="pwd", font=("Arial", 14))
pwd_input = tk.Entry(root, show="*", font=("Arial", 14))

# Sección de Confirmar pwd
confirmar_pwd_label = tk.Label(
  root, text="Confirmar pwd", font=("Arial", 14))
confirmar_pwd_input = tk.Entry(root, show="*", font=("Arial", 14)) 

# Sección de Botón
verificar_btn = tk.Button(root, text="Verificar",
  font=("Arial", 18), command=verificar)

# Sección donde muestra los errores
resultado_label = tk.Label(root, text="", font=("Arial", 14))

# Estructura y acomodados
label_aviso.pack(pady=10)
titulo_label.pack(pady=10)
correo_label.pack()
correo_input.pack(pady=10)
pwd_label.pack()
pwd_input.pack(pady=10)
confirmar_pwd_label.pack()
confirmar_pwd_input.pack(pady=10)
verificar_btn.pack(pady=20)
resultado_label.pack()

titulo_label.pack(pady=10)

root.mainloop()